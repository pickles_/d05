//
// Created by Dmitry Titenko on 11/6/17.
//

#ifndef _FORM_HPP
#define _FORM_HPP


#include <string>
#include <ostream>
#include "Bureaucrat.hpp"

class Bureaucrat;

class Form
{
	private:
		const std::string	_name;
		const int			_execGrade;
		const int			_signGrade;
		bool				_signed;

	public:
		Form();
		Form(const std::string &name, const int execGrade, const int signGrade);
		virtual ~Form();

		Form(const Form &);
		Form &operator=(const Form &);

		void beSigned(Bureaucrat &);

		const std::string &getName() const;
		int getExecGrade() const;
		int getSignGrade() const;
		bool isSigned() const;

		void setSigned(bool aSigned);

		class GradeTooHighException : public std::exception
		{
			public:
				GradeTooHighException();
				virtual ~GradeTooHighException() throw();

				GradeTooHighException(const GradeTooHighException &);
				GradeTooHighException &operator=(const GradeTooHighException &);

				virtual const char *what() const throw();
		};
		class GradeTooLowException : public std::exception
		{
			public:
				GradeTooLowException();
				virtual ~GradeTooLowException() throw();

				GradeTooLowException(const GradeTooLowException &);
				GradeTooLowException &operator=(const GradeTooLowException &);

				virtual const char *what() const throw();
		};
};

std::ostream &operator<<(std::ostream &os, const Form &form);




#endif //_FORM_HPP
