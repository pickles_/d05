//
// Created by Dmitry Titenko on 11/6/17.
//

#include <iostream>
#include "Bureaucrat.hpp"
#include "Form.hpp"

int main()
{
	std::cout << "-----Creating Bureaucrats-----" << std::endl;
	Bureaucrat elsa("Elsa", 1);
	Bureaucrat naomi("Naomi", 150);
	try
	{
		Bureaucrat alice("Alice", 151);
		std::cout << alice << std::endl;
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}

	std::cout << "-----trying to inc bureaucrat wit max grade-----" << std::endl;
	try
	{
		elsa.incGrade();
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
	std::cout << elsa << std::endl;


	std::cout << "-----trying to inc bureaucrat wit max grade-----" << std::endl;
	try
	{
		naomi.decGrade();
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
	std::cout << naomi << std::endl;
	std::cout << "-----copying bureaucrat and incrementing him-----" << std::endl;
	Bureaucrat naomi_new(naomi);
	std::cout << naomi_new << std::endl;
	naomi_new.incGrade();
	std::cout << naomi_new << "\n" << std::endl;

	Form f1("28B", 28, 28);
	Form f2("28C", 32, 150);
	Form f3("28D", 1, 149);

	std::cout << f1 << std::endl;
	std::cout << f2 << std::endl;
	std::cout << f3 << std::endl;

	try
	{
		Form f0("28A", 150, 0);
		std::cout << f0 << std::endl;
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}


	try
	{
		naomi.signForm(f3);
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
	std::cout << f3 << std::endl;

	try
	{
		naomi_new.signForm(f3);
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
	std::cout << f3 << std::endl;

	elsa.signForm(f3);

	try
	{
		naomi.signForm(f1);
		f1.beSigned(naomi);
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}


}