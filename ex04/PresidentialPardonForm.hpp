//
// Created by dtitenko on 11/7/17.
//

#ifndef D05_PRESIDENTIALPARDONFORM_HPP
#define D05_PRESIDENTIALPARDONFORM_HPP

#include "Form.hpp"
class PresidentialPardonForm : public Form
{
	public:
		PresidentialPardonForm();
		virtual ~PresidentialPardonForm();
		PresidentialPardonForm(const std::string &target);
		PresidentialPardonForm(const PresidentialPardonForm &);
		PresidentialPardonForm &operator=(const PresidentialPardonForm &);
		virtual void execute(const Bureaucrat &bureaucrat) const;
};

#endif //D05_PRESIDENTIALPARDONFORM_HPP
