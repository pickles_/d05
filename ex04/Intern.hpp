//
// Created by dtitenko on 11/7/17.
//

#ifndef D05_INTERN_HPP
#define D05_INTERN_HPP

#include "Form.hpp"
class Intern
{
	public:
		Intern();
		virtual ~Intern();

		Intern(const Intern &);
		Intern &operator=(const Intern &);

		Form *makeForm(const std::string &, const std::string &);
};

#endif //D05_INTERN_HPP
