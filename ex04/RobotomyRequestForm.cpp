//
// Created by dtitenko on 11/7/17.
//

#include <iostream>
#include <cstdlib>
#include "RobotomyRequestForm.hpp"
RobotomyRequestForm::RobotomyRequestForm()
		: Form("Robotomy Request", 45, 72)
{}

RobotomyRequestForm::~RobotomyRequestForm()
{}

RobotomyRequestForm::RobotomyRequestForm(const std::string &target)
		: Form("Robotomy Request", 45, 72)
{
	setTarget(target);
}

RobotomyRequestForm::RobotomyRequestForm(const RobotomyRequestForm &form)
		: Form(form)
{
	*this = form;
}

RobotomyRequestForm &RobotomyRequestForm::operator=(const RobotomyRequestForm &rhs)
{
	Form::operator=(rhs);
	return *this;
}

void RobotomyRequestForm::execute(const Bureaucrat &bureaucrat) const
{
	if (bureaucrat.getGrade() <= getExecGrade())
	{
		if (isSigned())
		{
			std::cout << "*drilling noises*" << std::endl;
			if (std::rand() % 2)
				std::cout << getTarget() <<" has been robotomized successfully" << std::endl;
			else
				std::cout << "It's failure." << std::endl;
		}
		else
			std::cout << *this << " is not signed yet." << std::endl;
	}
	else
		throw GradeTooLowException();
}
