//
// Created by dtitenko on 11/7/17.
//

#include <fstream>
#include <iostream>
#include "ShrubberyCreationForm.hpp"
ShrubberyCreationForm::ShrubberyCreationForm()
		: Form("Shrubbery Creation", 137, 145)
{}

ShrubberyCreationForm::~ShrubberyCreationForm()
{}

ShrubberyCreationForm::ShrubberyCreationForm(const std::string &target)
		: Form("Shrubbery Creation", 137, 145)
{
	setTarget(target);
}

ShrubberyCreationForm::ShrubberyCreationForm(const ShrubberyCreationForm &form)
		: Form(form)
{
	*this = form;
}

ShrubberyCreationForm &
ShrubberyCreationForm::operator=(const ShrubberyCreationForm &rhs)
{
	Form::operator=(rhs);
	return *this;
}
void ShrubberyCreationForm::execute(const Bureaucrat &bureaucrat) const
{
	static const char *tree[] = {
			"       _-_       ",
			"    /~~   ~~\\   ",
			" /~~         ~~\\",
			"{               }",
			" \\  _-     -_  /",
			"   ~  \\\\ //  ~ ",
			"_- -   | | _- _  ",
			"  _ -  | |   -_  ",
			"      // \\\\    "
	};

	std::string filename = getTarget() + "_shrubbery";
	if (bureaucrat.getGrade() <= getExecGrade())
	{
		if (isSigned())
		{
			std::ofstream ofs(filename.c_str());
			if (ofs.is_open())
			{
				size_t lines = sizeof(tree)/sizeof(tree[0]);
				for (size_t i = 0; i<lines; i++)
				{
					ofs << tree[i] << std::endl;
				}
				ofs.close();
			}
		}
		else
			std::cout << *this << " is not signed yet." << std::endl;
	}
	else
		throw GradeTooLowException();
}
