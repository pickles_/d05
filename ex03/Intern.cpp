//
// Created by dtitenko on 11/7/17.
//

#include <iostream>
#include "Intern.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"

Intern::Intern() {}
Intern::~Intern() {}
Intern::Intern(const Intern &intern) { *this = intern; }
Intern &Intern::operator=(const Intern &rhs)
{
	if (this == &rhs)
		return *this;
	return *this;
}

Form *Intern::makeForm(const std::string &formname, const std::string &target)
{
	Form *form = NULL;
	std::string fn;

	for (size_t i = 0; i < formname.size(); i++)
		fn += char(tolower(formname[i]));

	if (fn == "presidential pardon")
		form = new PresidentialPardonForm(target);
	else if (fn == "robotomy request")
		form = new RobotomyRequestForm(target);
	else if (fn == "shrubbery creation")
		form = new ShrubberyCreationForm(target);
	if (form)
		std::cout << "Intern creates " << fn << " form" <<std::endl;
	else
		std::cout << "Intern doesn't know how to create form " << fn << std::endl;
	return form;
}
