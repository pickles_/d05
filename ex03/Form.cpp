//
// Created by Dmitry Titenko on 11/6/17.
//

#include <iostream>
#include "Form.hpp"

Form::Form(): _execGrade(150), _signGrade(150), _signed(false)
{}

Form::Form(const std::string &name, const int execGrade, const int signGrade) : _name(name), _execGrade(execGrade),
																				_signGrade(signGrade), _signed(false)
{
	if (execGrade > 150 || signGrade > 150)
		throw GradeTooLowException();
	else if (execGrade < 1 || signGrade < 1)
		throw GradeTooHighException();
}

Form::~Form()
{

}

Form::Form(const Form &form) : _name(form._name),
							   _execGrade(form._execGrade),
							   _signGrade(form._signGrade)
{
	*this = form;
}

Form &Form::operator=(const Form &rhs)
{
	if (this == &rhs)
		return (*this);
	_signed = rhs._signed;
	_target = rhs._target;
	return (*this);
}

const std::string &Form::getName() const
{
	return _name;
}

int Form::getExecGrade() const
{
	return _execGrade;
}

int Form::getSignGrade() const
{
	return _signGrade;
}

bool Form::isSigned() const
{
	return _signed;
}

void Form::beSigned(Bureaucrat &bureaucrat)
{
	if (bureaucrat.getGrade() <= _signGrade)
	{
		if (!_signed)
		{
			std::cout << bureaucrat << " signs " << (*this) << std::endl;
			_signed = true;
		}
		else
			std::cout << bureaucrat << " cannot sign " << (*this) << " because it is already signed."<< std::endl;
	}
	else
		throw GradeTooLowException();
}

void Form::setSigned(bool aSigned)
{
	_signed = aSigned;
}
const std::string &Form::getTarget() const
{
	return _target;
}
void Form::setTarget(const std::string &target)
{
	_target = target;
}

std::ostream &operator<<(std::ostream &os, const Form &form)
{
	os << "Form " << form.getName() << "<exec grade = " << form.getExecGrade() <<
	   ", sign grade = " << form.getSignGrade() << ", signed: " << form.isSigned() << ">";
	return os;
}





Form::GradeTooLowException::GradeTooLowException() {}
Form::GradeTooLowException::~GradeTooLowException() throw() {}
Form::GradeTooLowException::GradeTooLowException(const GradeTooLowException &ex) { *this = ex; }
Form::GradeTooLowException &Form::GradeTooLowException::operator=(const GradeTooLowException &)
{ return *this; }
const char *Form::GradeTooLowException::what() const throw() { return "Grade is too low;"; }


Form::GradeTooHighException::GradeTooHighException() {}
Form::GradeTooHighException::~GradeTooHighException() throw() {}
Form::GradeTooHighException::GradeTooHighException(const GradeTooHighException &ex) { *this = ex; }
Form::GradeTooHighException &Form::GradeTooHighException::operator=(const GradeTooHighException &)
{ return *this; }
const char* Form::GradeTooHighException::what() const throw() { return "Grade is too high;"; }


