//
// Created by dtitenko on 11/7/17.
//

#ifndef _INTERN_HPP
#define _INTERN_HPP

#include "Form.hpp"
class Intern
{
	public:
		Intern();
		virtual ~Intern();

		Intern(const Intern &);
		Intern &operator=(const Intern &);

		Form *makeForm(const std::string &, const std::string &);
};

#endif //_INTERN_HPP
