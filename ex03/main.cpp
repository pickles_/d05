//
// Created by Dmitry Titenko on 11/6/17.
//

#include <iostream>
#include <cstdlib>
#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "Intern.hpp"

int main()
{
	Intern intern;
	Form *rrf, *ppf, *scf;

	rrf = intern.makeForm("robotomy request", "Bender");
	std::cout << *rrf << std::endl;

	scf = intern.makeForm("Shrubbery Creation","raspberries");
	std::cout << *scf << std::endl;

	ppf = intern.makeForm("Presidential ParDon", "Philip J. Fry");
	std::cout << *ppf << std::endl;

	delete ppf;
	delete rrf;
	delete scf;

	ppf = intern.makeForm("Presidential ParDonte", "Philip J. Fry");
}