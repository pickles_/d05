//
// Created by Dmitry Titenko on 11/6/17.
//

#ifndef D05_BUREAUCRAT_HPP
#define D05_BUREAUCRAT_HPP


#include <exception>
#include <string>
#include <ostream>
#include "Form.hpp"

class Form;

class Bureaucrat
{
	private:
		std::string _name;
		int _grade;

	public:
		Bureaucrat();
		Bureaucrat(const std::string &, int grade);
		virtual ~Bureaucrat();

		Bureaucrat(const Bureaucrat &);
		Bureaucrat &operator=(const Bureaucrat &);

		Bureaucrat &operator+=(int n);
		Bureaucrat &operator-=(int n);

		Bureaucrat &operator++();
		Bureaucrat &operator--();
		Bureaucrat operator++(int);
		Bureaucrat operator--(int);

		void incGrade();
		void decGrade();

		void signForm(Form &);
		void executeForm(const Form &);


		const std::string &getName() const;
		int getGrade() const;

		class GradeTooHighException : public std::exception
		{
			public:
				GradeTooHighException();
				virtual ~GradeTooHighException() throw();

				GradeTooHighException(const GradeTooHighException &);
				GradeTooHighException &operator=(const GradeTooHighException &);

				virtual const char *what() const throw();
		};
		class GradeTooLowException : public std::exception
		{
			public:
				GradeTooLowException();
				virtual ~GradeTooLowException() throw();

				GradeTooLowException(const GradeTooLowException &);
				GradeTooLowException &operator=(const GradeTooLowException &);

				virtual const char *what() const throw();
		};
};

std::ostream &operator<<(std::ostream &os, const Bureaucrat &bureaucrat);

#endif //D05_BUREAUCRAT_HPP
