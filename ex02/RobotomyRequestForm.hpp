//
// Created by dtitenko on 11/7/17.
//

#ifndef D05_ROBOTOMYREQUESTFORM_HPP
#define D05_ROBOTOMYREQUESTFORM_HPP

#include "Form.hpp"

class RobotomyRequestForm : public Form
{
	public:
		RobotomyRequestForm();
		virtual ~RobotomyRequestForm();
		RobotomyRequestForm(const std::string &);
		RobotomyRequestForm(const RobotomyRequestForm &);
		RobotomyRequestForm &operator=(const RobotomyRequestForm &);
		virtual void execute(const Bureaucrat &bureaucrat) const;
};

#endif //D05_ROBOTOMYREQUESTFORM_HPP
