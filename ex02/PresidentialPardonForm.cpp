//
// Created by dtitenko on 11/7/17.
//

#include <iostream>
#include "PresidentialPardonForm.hpp"

PresidentialPardonForm::PresidentialPardonForm()
		: Form("Presidential Pardon", 5, 25)
{
}

PresidentialPardonForm::~PresidentialPardonForm()
{}

PresidentialPardonForm::PresidentialPardonForm(const std::string &target)
		: Form("Presidential Pardon", 5, 25)
{
	setTarget(target);
}

PresidentialPardonForm::PresidentialPardonForm(const PresidentialPardonForm &form)
	: Form(form)
{ *this = form; }

PresidentialPardonForm &
PresidentialPardonForm::operator=(const PresidentialPardonForm &rhs)
{
	Form::operator=(rhs);
	return *this;
}

void PresidentialPardonForm::execute(const Bureaucrat &bureaucrat) const
{
	if (bureaucrat.getGrade() <= getExecGrade())
	{
		if (isSigned())
			std::cout << getTarget() << " has been pardoned by Zafod Beeblebrox." << std::endl;
		else
			std::cout << *this << " is not signed yet." << std::endl;
	}
	else
		throw GradeTooLowException();
}

