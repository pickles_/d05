//
// Created by Dmitry Titenko on 11/6/17.
//

#include <iostream>
#include <cstdlib>
#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"

int main()
{
	std::srand(time(NULL));
	std::cout << "-----Creating Bureaucrats-----" << std::endl;
	Bureaucrat elsa("Elsa", 1);
	Bureaucrat naomi("Naomi", 150);

	try
	{
		Bureaucrat alice("Alice", 151);
		std::cout << alice << std::endl;
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}

	std::cout << "-----trying to inc bureaucrat wit max grade-----" << std::endl;
	try
	{
		elsa.incGrade();
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
	std::cout << elsa << std::endl;


	std::cout << "-----trying to inc bureaucrat wit max grade-----" << std::endl;
	try
	{
		naomi.decGrade();
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
	std::cout << naomi << std::endl;
	std::cout << "-----copying bureaucrat and incrementing him-----" << std::endl;
	Bureaucrat naomi_new(naomi);
	std::cout << naomi_new << std::endl;
	naomi_new.incGrade();
	std::cout << naomi_new << "\n" << std::endl;

	PresidentialPardonForm f1("sgshjkl");
	RobotomyRequestForm f2("sadasddsadsadasdsads");
	ShrubberyCreationForm f3("sadasds");

	std::cout << f1 << std::endl;
	std::cout << f2 << std::endl;
	std::cout << f3 << std::endl;

	try
	{
		ShrubberyCreationForm f0("28A");
		std::cout << f0 << std::endl;
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}


	try
	{
		naomi.signForm(f3);
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
	std::cout << f3 << std::endl;

	try
	{
		naomi_new.signForm(f3);
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
	std::cout << f3 << std::endl;

	elsa.signForm(f3);

	try
	{
		naomi.signForm(f1);
		f1.beSigned(naomi);
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}

	try
	{
		naomi.executeForm(f1);
		f1.execute(naomi);
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}

	try
	{
		elsa.executeForm(f1);
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
	elsa.signForm(f1);
	elsa.signForm(f2);
	elsa.signForm(f3);
	elsa.executeForm(f1);
	elsa.executeForm(f2);
	elsa.executeForm(f3);
}