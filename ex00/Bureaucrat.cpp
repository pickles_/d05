//
// Created by Dmitry Titenko on 11/6/17.
//

#include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat() : _grade(150)
{}

Bureaucrat::Bureaucrat(const std::string &name, int grade) : _name(name), _grade(grade)
{
	if (grade > 150)
		throw GradeTooLowException();
	else if (grade < 1)
		throw GradeTooHighException();
}

Bureaucrat::~Bureaucrat()
{}

Bureaucrat::Bureaucrat(const Bureaucrat &bureaucrat)
{
	*this = bureaucrat;
}


Bureaucrat &Bureaucrat::operator=(const Bureaucrat &rhs)
{
	if (this == &rhs)
		return (*this);
	_name = rhs._name;
	_grade = rhs._grade;
	return (*this);
}

std::ostream &operator<<(std::ostream &os, const Bureaucrat &bureaucrat)
{
	os << bureaucrat.getName() << ", bureaucrat grade " << bureaucrat.getGrade();
	return os;
}



Bureaucrat &Bureaucrat::operator+=(int n)
{
	if (_grade - n < 1)
		throw Bureaucrat::GradeTooHighException();
	_grade -= n;
	return *this;
}

Bureaucrat &Bureaucrat::operator-=(int n)
{
	if (_grade + n > 150)
		throw Bureaucrat::GradeTooLowException();
	_grade += n;
	return *this;
}

Bureaucrat &Bureaucrat::operator++()
{
	return (*this += 1);
}

Bureaucrat &Bureaucrat::operator--()
{
	return (*this -= 1);
}

Bureaucrat Bureaucrat::operator++(int)
{
	Bureaucrat b(*this);
	(*this) += 1;
	return b;
}

Bureaucrat Bureaucrat::operator--(int)
{
	Bureaucrat b(*this);
	(*this) -= 1;
	return b;
}




void Bureaucrat::incGrade()
{
	(*this)++;
}

void Bureaucrat::decGrade()
{
	(*this)--;
}




const std::string &Bureaucrat::getName() const
{
	return _name;
}

int Bureaucrat::getGrade() const
{
	return _grade;
}


Bureaucrat::GradeTooLowException::GradeTooLowException() {}
Bureaucrat::GradeTooLowException::~GradeTooLowException() throw() {}
Bureaucrat::GradeTooLowException::GradeTooLowException(const GradeTooLowException &ex) { *this = ex; }
Bureaucrat::GradeTooLowException& Bureaucrat::GradeTooLowException::operator=(const GradeTooLowException &)
{ return *this; }
const char *Bureaucrat::GradeTooLowException::what() const throw() { return "Grade is too low; Min grade is 150;"; }




Bureaucrat::GradeTooHighException::GradeTooHighException() {}
Bureaucrat::GradeTooHighException::~GradeTooHighException() throw() {}
Bureaucrat::GradeTooHighException::GradeTooHighException(const GradeTooHighException &ex) { *this = ex; }
Bureaucrat::GradeTooHighException& Bureaucrat::GradeTooHighException::operator=(const GradeTooHighException &)
{ return *this; }
const char* Bureaucrat::GradeTooHighException::what() const throw() { return "Grade is too high; Max grade is 1;"; }