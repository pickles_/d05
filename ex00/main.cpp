//
// Created by Dmitry Titenko on 11/6/17.
//

#include <iostream>
#include "Bureaucrat.hpp"

int main()
{
	std::cout << "-----Creating Bureaucrats-----" << std::endl;
	Bureaucrat elsa("Elsa", 1);
	Bureaucrat naomi("Naomi", 150);
	try
	{
		Bureaucrat alice("Alice", 151);
		std::cout << alice << std::endl;
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}

	std::cout << "-----trying to inc bureaucrat wit max grade-----" << std::endl;
	try
	{
		elsa.incGrade();
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
	std::cout << elsa << std::endl;


	std::cout << "-----trying to inc bureaucrat wit max grade-----" << std::endl;
	try
	{
		naomi.decGrade();
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
	std::cout << naomi << std::endl;
	std::cout << "-----copying bureaucrat and incrementing him-----" << std::endl;
	Bureaucrat naomi_new(naomi);
	std::cout << naomi_new << std::endl;
	naomi_new.incGrade();
	std::cout << naomi_new << std::endl;
}